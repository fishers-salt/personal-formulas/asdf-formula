# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as asdf with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('asdf-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_asdf', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

asdf-config-file-zsh-env-include-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/zsh/env_includes
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - asdf-package-install-user-{{ name }}-present

asdf-config-file-zsh-rc-include-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/zsh/rc_includes
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - asdf-package-install-user-{{ name }}-present

asdf-config-file-zsh-env-includes-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/zsh/env_includes/asdf.zsh
    - source: {{ files_switch([
                  name ~ '-asdf_env.zsh.tmpl',
                  'asdf_env.zsh.tmpl'],
                lookup='asdf-config-file-zsh-env-includes-config-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - asdf-config-file-zsh-env-include-dir-{{ name }}-managed

asdf-config-file-zsh-rc-includes-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/zsh/rc_includes/asdf.zsh
    - source: {{ files_switch([
                  name ~ '-asdf_rc.zsh.tmpl',
                  'asdf_rc.zsh.tmpl'],
                lookup='asdf-config-file-zsh-rc-includes-config-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - asdf-config-file-zsh-rc-include-dir-{{ name }}-managed

asdf-config-file-base-tool-versions-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.tool-versions
    - source: {{ files_switch([
                  name ~ '-top-tool-versions.tmpl',
                  'top-tool-versions.tmpl'],
                lookup='asdf-config-file-base-tool-versions-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - asdf-package-install-user-{{ name }}-present
{% endif %}
{% endfor %}
{% endif %}
