# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as asdf with context %}

{% if salt['pillar.get']('asdf-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_asdf', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

asdf-config-clean-base-tool-versions-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.tool-versions

asdf-config-clean-zsh-rc-includes-config-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/zsh/rc_includes/asdf.zsh

asdf-config-clean-zsh-env-includes-config-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/zsh/env_includes/asdf.zsh

{% endif %}
{% endfor %}
{% endif %}
