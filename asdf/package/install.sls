# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as asdf with context %}

asdf-package-install-git-pkg-installed:
  pkg.installed:
    - name: {{ asdf.gitpkg.name }}

{% if salt['pillar.get']('asdf-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_asdf', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

asdf-package-install-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

asdf-package-install-local-share-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.local/share
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - asdf-package-install-user-{{ name }}-present

asdf-package-install-repo-{{ name }}-cloned:
  git.latest:
    - name: https://github.com/asdf-vm/asdf.git
    - target: {{ home }}/.local/share/asdf
    - rev: v0.11.3
    - user: {{ name }}
    - require:
      - asdf-package-install-local-share-dir-{{ name }}-managed
      - asdf-package-install-git-pkg-installed
      - asdf-package-install-user-{{ name }}-present

{# asdf is cloned with ownership stooj:root #}
asdf-package-install-asdf-ownership-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.local/share/asdf
    - user: {{ name }}
    - group: {{ user_group }}
    - recurse:
      - user
      - group
    - require:
      - asdf-package-install-repo-{{ name }}-cloned
      - asdf-package-install-user-{{ name }}-present

{% endif %}
{% endfor %}
{% endif %}
