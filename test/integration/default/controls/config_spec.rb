# frozen_string_literal: true

control 'asdf-config-file-zsh-env-include-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/zsh/env_includes') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'asdf-config-file-zsh-rc-include-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/zsh/rc_includes') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'asdf-config-file-zsh-env-includes-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/zsh/env_includes/asdf.zsh') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('ASDF_DATA_DIR="$HOME"/.local/share/asdf') }
    its('content') { should include('export ASDF_DATA_DIR') }
  end
end

control 'asdf-config-file-zsh-rc-includes-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/zsh/rc_includes/asdf.zsh') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('source "$HOME"/.local/share/asdf/asdf.sh') }
  end
end

control 'asdf-config-file-base-tool-versions-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.tool-versions') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('ruby system') }
  end
end
