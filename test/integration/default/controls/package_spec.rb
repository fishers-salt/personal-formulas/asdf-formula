# frozen_string_literal: true

control 'asdf-package-install-git-pkg-installed' do
  title 'git should be installed'

  describe package('git') do
    it { should be_installed }
  end
end

control 'asdf-package-install-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'asdf-package-install-local-share-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.local/share') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end
control 'asdf-package-install-repo-auser-cloned' do
  title 'should be cloned'

  describe file('/home/auser/.local/share/asdf/ballad-of-asdf.md') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    its('content') { should include('*This was the mail I wrote to a few friends') }
  end
end

control 'asdf-package-install-asdf-ownership-auser-managed' do
  title 'should belong to the correct group'
  describe file('/home/auser/.local/share/asdf/ballad-of-asdf.md') do
    it { should be_grouped_into 'auser' }
  end
end
