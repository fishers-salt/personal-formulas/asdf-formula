# frozen_string_literal: true

control 'asdf-config-clean-base-tool-versions-{{ name }}-absent' do
  title 'should be absent'

  describe file('/home/auser/.tool-versions') do
    it { should_not exist }
  end
end

control 'asdf-config-clean-zsh-rc-includes-config-{{ name }}-absent' do
  title 'should be absent'

  describe file('/home/auser/.config/zsh/rc_includes/asdf.zsh') do
    it { should_not exist }
  end
end

control 'asdf-config-clean-zsh-env-includes-config-{{ name }}-absent' do
  title 'should be absent'

  describe file('/home/auser/.config/zsh/env_includes/asdf.zsh') do
    it { should_not exist }
  end
end
