# frozen_string_literal: true

control 'asdf-package-clean-repo-auser-absent' do
  title 'should be absent'

  describe directory('/home/auser/.local/share/asdf') do
    it { should_not exist }
  end
end
